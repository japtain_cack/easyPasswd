#!/usr/bin/env bash

numPasswd=3
numWords=2
maxLen=8
minLen=4
rndNumPad=2
preesuffPad=2
useNatural=false
diffPresuff=false
diffRndNum=true
presuffList='~!@#$%^&*()=_+<>.?/\|[]{}-'
delimiterList=".,_/+-"

wordList=()
rndWords=()
presuffix=''
delimiter=''
args=("$@")
version=1.0.0

license="
easyPasswd v${version}
Copyright (C) 2017  Nathan Snow

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

help="
Usage: $0 [options] [--help|--version]

[options]
\t--numpasswds    Number of passwords to generate.
\t--numwords      Number of words used in the password.
\t--maxlen        Maximum length of words.
\t--minlen        Minimum length of words.
\t--numpad        n number of digits used in padding the password.
\t--charpad       n number of characters used in padding the password.
\t--diffnumpad    The starting and ending digit padding will be different.
\t--diffcharpad   The starting and ending character padding will be different.
\t--delim         Delimiter used to seperate password components. Can be a list of characters. if using a '-', it must be last in the list.
\t--natural       Generate pseudorandom natural sounding words instead of dictionary words (faster but may be harder to remember).
"


for ((arg=0;arg<"${#args[@]}";arg++)); do
	#echo ${args[$arg]}
	[ "${args[$arg]}" == "--version" ] && echo -e "${license}" && exit
	[ "${args[$arg]}" == "--help" ] && echo -e "${help}" && exit
	[ "${args[$arg]}" == "--numpasswds" ] && numPasswd=${args[$arg+1]}
	[ "${args[$arg]}" == "--numwords" ] && numWords=${args[$arg+1]}
	[ "${args[$arg]}" == "--maxlen" ] && maxLen=${args[$arg+1]}
	[ "${args[$arg]}" == "--minlen" ] && minLen=${args[$arg+1]}
	[ "${args[$arg]}" == "--numpad" ] && rndNumPad=${args[$arg+1]}
	[ "${args[$arg]}" == "--charpad" ] && preesuffPad=${args[$arg+1]}
	[ "${args[$arg]}" == "--delim" ] && delimiterList=${args[$arg+1]}
	[ "${args[$arg]}" == "--natural" ] && useNatural=true
	[ "${args[$arg]}" == "--diffnumpad" ] && diffPresuff="${args[$arg+1]}"
	[ "${args[$arg]}" == "--diffcharpad" ] && diffRndNum="${args[$arg+1]}"
	#[ "${args[$arg]}" == "--" ] && echo ${args[$arg]}
done


if [ -f /usr/share/dict/words ] && [ ! "${useNatural}" == "true" ]; then
	rndWords=("$(shuf /usr/share/dict/words)")
else
	echo [Info] Using natural word generation method...
	useNatural=true
fi

rndNum() {
    rndNums=''
    rndNums=$(tr -dc '0-9' < /dev/urandom | head -c ${rndNumPad} | xargs)
}

rndDelim() {
    delimiter=''
    local lvar=$(tr -dc "${delimiterList}" < /dev/urandom | head -c 1 | xargs)
    for ((c=0;c<1;c++)); do
        delimiter+=${lvar}
    done
}

presuff() {
    presuffix=''
    local lvar=$(tr -dc "${presuffList}" < /dev/urandom | head -c 1 | xargs)
    for ((c=0;c<${preesuffPad};c++)); do
        presuffix+=${lvar}
    done
}

getNaturalWords() {
    rndWords=()
    for i in {1..1000}; do
        cons=(b c d f g h j k l m n p r s t v w x z pt gl gr ch ph ps sh st th wh)
        conscs=(ck cm dr ds ft gh gn kr ks ls lt lr mp mt ms ng ns rd rg rs rt ss ts tch)
        vows=(a e i o u y ee oa oo)
    
        len=$((($maxLen+0 == 0) ? 6 : $maxLen+0))
        alt=$RANDOM
        word=
    
        while [ ${#word} -lt $len ]; do
    
            if [ $(($alt%2)) -eq 0 ]; then
                rc=${cons[(($RANDOM%${#cons[*]}))]}
            else
                rc=${vows[(($RANDOM%${#vows[*]}))]}
            fi

            if [ $((${#word}+${#rc})) -gt $len ]; then continue; fi

            word=$word$rc

            ((alt++))

            if [ ${#conscs[*]} -gt 0 ]; then
                cons=(${cons[@]} ${conscs[@]})
                conscs=()
            fi
        done
        
        rndWords+=("$word")
    done
}

getWords() {
    wordList=()
    for item in ${rndWords[@]}; do
        if [ ${#item} -le $maxLen ] && [ ${#item} -ge $minLen ]; then
            wordList+=("$item")
        fi
    done
}

mkPasswd() {
    local myPasswd=''
    local wordCount=0
    [ "${useNatural}" == "true" ] && getNaturalWords
    getWords
    
    for ((i=0;i<${numPasswd};i++)); do
	myPasswd=''
        rndNum
        presuff
        rndDelim

        myPasswd=${presuffix}${rndNums}${delimiter}
        for ((ii=0;ii<=${numWords}-1;ii++)); do
            myPasswd+=${wordList[$ii+$i+$wordCount]}${delimiter}
            wordCount=$wordCount+$numWords
        done

        [ "${diffRndNum}" == "true" ] && rndNum
	[ "${diffPresuff}" == "true" ] && presuff

        myPasswd+=${rndNums}${presuffix}
        echo Password$(($i + 1)): ${myPasswd}
    done
}

mkPasswd
