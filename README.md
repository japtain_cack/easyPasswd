Overview:
==============
Attempting to reproduce the functionality of https://xkpasswd.net/s/ using pure bash.

Usage:
==============
    Usage: $0 [options] [--help|--version]
    
    [options]
	    --numpasswds    Number of passwords to generate.
    	--numwords      Number of words used in the password.
    	--maxlen        Maximum length of words.
    	--minlen        Minimum length of words.
    	--numpad        n number of digits used in padding the password.
    	--charpad       n number of characters used in padding the password.
    	--diffnumpad    The starting and ending digit padding will be different.
    	--diffcharpad   The starting and ending character padding will be different.
    	--delim         Delimiter used to seperate password components.
    	--natural       Generate psudorandom natural sounding words instead of dictionary words (faster but may be harder to remember).

Install:
================
bash$ `git clone https://gitlab.com/Jedimaster0/easyPasswd.git`

bash$ `cd easyPasswd`

bash$ `./easyPasswd.sh`

Special Thanks:
===============
https://github.com/mattinclude for the natural word generation.